const fs = require('fs');

const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if(url === '/') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>\n' + 
                        '<head>\n' +
                            '<title>Enter Message</title>\n' +
                        '</head>\n' +
                        '<body>\n' +
                            '<form action="/message" method="POST">\n' +
                                '<input type="text" name="message">\n' +
                                '<button type="Submit">Send</button>\n' +
                            '</form>\n' +
                        '</body>\n' +
                    '</html>');
        return res.end();
    }
    if(url === '/message' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
            console.log(chunk);
            body.push(chunk);
        });
        req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
            fs.writeFile('message.txt', message, err => {
                res.statusCode = 302;
                res.setHeader('Location', '/');
                res.end();
            });
        });
    }
}

// module.exports = requestHandler;

// module.exports = {
//     handler: requestHandler,
//     someText: 'Some hard coded text'
// };

module.exports.handler = requestHandler;